* * * 
# CloudWatch Event Schemas
* * *

#### Schemas provided because of the lack of publically available examples of specific CloudWatch log event schemas.

* * *

### Event:  RunInstances

CloudWatch log event trigged when EC2 RunInstances API called.
RunInstances is called whenever a new EC2 instance is created
from an Amazon Machine Image (AMI).  See [cwEvent-RunInstances.json](./cwEvent-RunInstances.json)

* * *

### Event:  StartInstances

CloudWatch log event trigged when EC2 StartInstances API called. 
StartInstances is called when a previously stopped instance
start is attempted.  See [cwEvent-StartInstances](./cwEvent-StartInstances.json)

* * *
